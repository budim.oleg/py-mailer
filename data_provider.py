from os.path import exists
import requests
import csv
import re


class InvalidDataArgumentException(Exception):
    ...


class DataProvider(object):

    is_api_sourse = False

    def __init__(self, path: str = None):
        if not path:
            raise InvalidDataArgumentException('Data file path not provided')

        if isinstance(path, dict):
            self.__init_api_data(path)
        else:
            if not exists(path):
                raise InvalidDataArgumentException(
                    'Data file {} not found'.format(path)
                )

            self.csv_file_path = path

    def get_data(self):
        if self.is_api_sourse:
            return self.__read_from_api()

        return self.__read_from_file()

    def mark_as_send(self, data: dict):
        if not self.is_api_sourse:
            return

        id = data.get('_ID')

        if not id:
            return

        r = requests.post(
            f'{self.api_url}/mark_as_sent/{id}'
        )

        if 200 != r.status_code:
            print(
                'Email {}, id {} - mark as sent faild'.format(
                    data.get('EMAIL'),
                    id
                )
            )

    def __init_api_data(self, conf: dict):
        url = conf.get('url')
        type = conf.get('type')
        self.domains = list(conf.get('domains', []))

        if not url:
            raise InvalidDataArgumentException(
                'Data url not provided: {}'.format(conf)
            )
        if not type:
            raise InvalidDataArgumentException(
                'Data type not provided: {}'.format(conf)
            )

        if not re.fullmatch(r'^(http|https)://.+', url):
            raise InvalidDataArgumentException(
                'Data url not valid: {}'.format(conf)
            )

        self.is_api_sourse = True
        self.api_url = '{}/'.format(str(url).rstrip('/'))
        self.type = str(type)

    def __read_from_file(self):
        with open(self.csv_file_path, 'r') as file:
            headers = file.readline().split(',')
            headers[len(headers) - 1] = headers[len(headers) - 1][:-1]

        # i am opening the csv file two times above and below INTENTIONALLY, changing will cause error
        with open(self.csv_file_path, 'r') as file:
            data = csv.DictReader(file)
            for row in data:
                yield row['EMAIL'], row

    def __read_from_api(self):
        while True:
            params = {}

            if self.domains:
                params['domains'] = self.domains

            result = requests.get(f'{self.api_url}get/{self.type}', params)

            if 200 != result.status_code:
                break

            email_data = dict(result.json()).get('email')

            if not email_data:
                break

            data = email_data.get('data', {})
            email = email_data.get('email')
            id = email_data.get('_id')

            data['EMAIL'] = email
            data['_ID'] = id

            yield email, data
