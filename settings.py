import json
from typing import List
from data_provider import DataProvider
from template import Template

from sender import Sender


class InvalidDataException(Exception):
    ...


def init(config_name: str):
    AC_LIST: List = []
    TEMPLATES_LIST: List = []

    conf_file = f'{config_name}.json'
    template_conf_file = None
    data_provider = None
    general_send_limit = None

    try:
        with open(conf_file) as json_file:
            conf = json.load(json_file)

        template_conf_file = dict(conf).get('templates_conf')
        data_provider = dict(conf).get('data_file')
        senders = dict(conf).get('senders')
        general_send_limit = dict(conf).get('general_send_limit')

    except FileNotFoundError:
        print('File {} not found'.format(conf_file))
    except Exception as e:
        print('Error: {}'.format(e))

    try:
        data_provider = DataProvider(data_provider)
    except Exception as e:
        data_provider = None
        print('Data file error: {}'.format(str(e)))

    # Configure templates list
    if data_provider:
        try:
            with open(str(template_conf_file)) as json_file:
                data = json.load(json_file)

            if not isinstance(data, List):
                raise InvalidDataException()

            for conf in data:
                try:
                    tpl = Template(conf)

                    TEMPLATES_LIST.append(tpl)
                except Exception as e:
                    print('Message confifuration {} not valid. Error'.format(
                        conf), str(e))

        except FileNotFoundError:
            print('File {} not found'.format(template_conf_file))
        except InvalidDataException:
            print('Messages in {} should be set as list'.format(template_conf_file))
        else:
            print('Messages configuration loaded successfully')

    # Configure senders
    if TEMPLATES_LIST and data_provider:
        try:
            if not isinstance(senders, List):
                raise InvalidDataException()

            for conf in senders:
                try:
                    sender = Sender(conf)
                    sender.test()

                    AC_LIST.append(sender)
                except Exception as e:
                    print('Confifuration {} not valid. Error'.format(conf), str(e))

        except FileNotFoundError:
            print('File {} not found'.format(conf_file))
        except InvalidDataException:
            print('Accounts in {} should be set as list'.format(conf_file))
        else:
            print('Accounts credentials loaded successfully')

    return AC_LIST, TEMPLATES_LIST, data_provider, general_send_limit
