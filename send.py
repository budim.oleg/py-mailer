import csv
import os
import random
import argparse
import settings
import random
import string
import re
from sender import Sender
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from template import Template
from datetime import datetime
from itertools import cycle


def prRed(skk): print("\033[91m{}\033[00m" .format(skk))
def prGreen(skk): print("\033[92m{}\033[00m" .format(skk))
def prYellow(skk): print("\033[93m{}\033[00m" .format(skk))


def get_data_from_template(row: dict, template: Template):

    subject = template.subject
    text = template.txt_body
    html = template.html_body

    html, text = text_randomize(html, text)
    text, _ = text_randomize(text)
    subject, _ = text_randomize(subject)

    letters = string.ascii_lowercase
    num_l = round(random.uniform(8, 20))
    rand = ''.join(random.choice(letters) for i in range(num_l))

    for header in row:
        value = row[header]

        if subject:
            subject = subject.replace(f'${header}', value)
        if text:
            text = text.replace(f'${header}', value)
        if html:
            html = html.replace(f'${header}', value)

    if html:
        html = html.replace(f'$RANDOM', rand)
        html = html.replace(f'$_SUBJECT', subject)
    if text:
        text = text.replace(f'$RANDOM', rand)
        text = text.replace(f'$_SUBJECT', subject)

    return subject, text, html


def send_emails(recepient: str, row: dict, server: Sender, template: Template):
    subject, text, html = get_data_from_template(row, template)

    multipart_msg = MIMEMultipart('alternative')

    multipart_msg['Subject'] = subject
    multipart_msg['To'] = recepient

    v1 = round(random.uniform(0, 7))
    v2 = round(random.uniform(0, 20))
    v3 = round(random.uniform(1, 20))

    p = random.choice(['Microsoft MimeOLE', 'Mailer', 'Sender'])

    multipart_msg['X-Priority'] = '2'
    multipart_msg['X-MSMail-Priority'] = 'High'
    multipart_msg['X-MimeOLE'] = f'Produced By {p} v{v1}.{v2}.{v3}'

    multipart_msg['List-Unsubscribe-Post'] = 'List-Unsubscribe=One-Click'
    multipart_msg['List-Unsubscribe'] = f'<mailto:{server.email}?subject=unsubscribe>'

    part1 = MIMEText(str(text), 'plain')
    part2 = MIMEText(str(html), 'html')

    multipart_msg.attach(part1)
    multipart_msg.attach(part2)

    # embed images
    if len(template.embed_images):
        for img in template.embed_images:
            multipart_msg.attach(img)

    # add attachments
    if template.attachments:
        for content, name in zip(template.attachments['contents'], template.attachments['names']):
            attach_part = MIMEBase('application', 'octet-stream')
            attach_part.set_payload(content)
            encoders.encode_base64(attach_part)
            attach_part.add_header(
                'Content-Disposition',
                f"attachment; filename={name}"
            )
            multipart_msg.attach(attach_part)
    try:
        server.send(multipart_msg)
    except Exception as err:
        prYellow(
            f'Problem occurend while sending to {recepient} from {server.email}: Error: {err}')
    else:
        return True


def print_sender_stat(sender: Sender):
    prGreen(f'Sent {sender.sent_count} emails from {sender.email}')


def text_randomize(text: str, text2: str = None):
    blocs = re.findall('{([^\}]+)}', str(text))

    if not blocs:
        return text, text2

    for bloc in blocs:
        phrase = random.choice(bloc.split('|'))
        text = text.replace(f'{{{bloc}}}', phrase)

        if text2:
            text2 = text2.replace(f'{{{bloc}}}', phrase)

    return text, text2


if __name__ == "__main__":
    # Налаштування аргументів командного рядка
    parser = argparse.ArgumentParser(
        'Send bulk emails'
    )
    parser.add_argument(
        "--config-name",
        help="Name of config json file",
        default="config",
        type=str
    )
    args = parser.parse_args()

    config_name = args.config_name

    AC_LIST, TEMPLATES_LIST, data_provider, send_limit = settings.init(
        config_name
    )

    sent_count = 0

    time = datetime.now().strftime("%d%m%Y_%H%M%S")
    result_file = f'results/result_{time}.csv'
    f = open(result_file, 'w')
    writer = csv.writer(f)
    writer.writerow(['EMAIL', 'IS SENT'])

    try:
        if not data_provider:
            raise Exception('Data not found')

        if not len(AC_LIST):
            raise Exception('No active senders found')

        if not len(TEMPLATES_LIST):
            raise Exception('No templates found')

        _AC_LIST = cycle(AC_LIST)
        # TEMPLATES_LIST = cycle(TEMPLATES_LIST)

        for recepient, row in data_provider.get_data():
            if not len(AC_LIST):
                raise Exception('No active senders found')

            sender: Sender = next(_AC_LIST)
            template = random.choice(TEMPLATES_LIST)

            if send_emails(recepient, row, sender, template):
                sent_count += 1
                print(f'Send {sent_count}\r', end="")

                data_provider.mark_as_send(row)

                writer.writerow([recepient, '1'])

                if sender.is_limit_reached():
                    print_sender_stat(sender)
                    AC_LIST.remove(sender)
                    _AC_LIST = cycle(AC_LIST)
            else:
                writer.writerow([recepient, '0'])
                if sender.is_blocked():
                    print_sender_stat(sender)
                    AC_LIST.remove(sender)
                    _AC_LIST = cycle(AC_LIST)

            if send_limit and sent_count >= send_limit:
                break
            
    except Exception as e:
        prRed(f"Error: {e}")

    f.close()

    for sender in AC_LIST:
        print_sender_stat(sender)

    prGreen(f"A total of {sent_count} emails were sent")
    prYellow(f"The results are exported to the {result_file} file")
