from asyncio.proactor_events import constants
from email.mime.base import MIMEBase
from email.utils import formataddr
from smtplib import SMTP, SMTP_SSL
import datetime
import time

CONST_TIMEOUT = 5


class InvalidSenderArgumentException(Exception):
    ...


class SenderSendMailException(Exception):
    ...


class Sender(object):
    __sent_count = 0
    __sent_error = 0
    __send_time = None

    def __init__(self, config: dict):
        self.__config = config
        self.__limit = int(config.get('limit', 0))
        self.__login = config.get('sender_login')
        self.__email = config.get('sender_email')
        self.__replay_to = config.get('replay_to')
        self.__delay_timeout = int(config.get('delay_timeout', CONST_TIMEOUT))

    @property
    def sent_count(self):
        return self.__sent_count

    @property
    def email(self):
        if self.__email:
            return self.__email

        return self.__login

    def is_limit_reached(self) -> bool:
        if not self.__limit:
            return False

        return self.__sent_count >= self.__limit

    def is_blocked(self) -> bool:
        return self.__sent_error >= 3

    def send(self, message: MIMEBase):
        if self.is_limit_reached():
            raise SenderSendMailException(
                f'{self.__limit} message limit reached')

        if self.is_blocked():
            raise SenderSendMailException('Send message blocked')

        if self.__send_time:
            date = datetime.datetime.utcnow()
            delta = datetime.timedelta(seconds=self.__delay_timeout)

            if self.__send_time + delta > date:
                delay = (self.__send_time + delta - date).total_seconds()
                time.sleep(delay)

        self.__send_time = datetime.datetime.utcnow()

        name = self.__config.get('sender_name')

        message['From'] = formataddr((name, self.email))

        if self.__replay_to:
            message['Reply-To'] = formataddr((name, self.__replay_to))

        try:
            server = self.__connect()
            server.send_message(message, from_addr=self.email)
        except Exception as err:
            if (str(err).find('Daily user sending quota exceeded') != -1):
                self.__sent_error = 3
                raise SenderSendMailException(
                    'Daily user sending quota exceeded')
            else:
                self.__sent_error += 1

            raise SenderSendMailException(str(err))
        else:
            self.__sent_count += 1

        server.quit()

    def test(self):
        server = self.__connect()
        server.quit()

    def __connect(self) -> SMTP:
        try:
            host = self.__config.get('smtp_host')
            assert host, 'SMTP host not defined'

            port = self.__config.get('smtp_port')

            if 'localhost' != host:
                assert port, 'SMTP port not defined'

            if not port:
                port = 0

            assert self.email, 'Sender email not defined'

            if self.__login:
                password = self.__config.get('sender_password')
                assert password, 'Sender password not defined'

            ssl = self.__config.get('smtp_use_ssl', False)
            tls = self.__config.get('smtp_use_tls', False)

            if ssl:
                server = SMTP_SSL(host=host, port=port, timeout=30)
            else:
                server = SMTP(host=host, port=port, timeout=30)
            server.connect(host=host, port=port)
            server.ehlo()

            if tls:
                server.starttls()
                server.ehlo()

            if self.__login:
                server.login(user=self.__login, password=password)

            return server

        except AssertionError as e:
            raise InvalidSenderArgumentException(str(e))
