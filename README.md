# py email sender

Автоматична відправка електронних листів 

## Характеристики 

Це проста програма для відправки електронних листів

1. Надсилайте динамічні електронні листи з необмеженою кількістю змінних, витягуючи дані з файлу CSV файлу.
2. Підтримує відправку листів із вбудованими в тіло повідомлення зображеннями
3. Підтримує прикріплення будь-яких файлів.

## Використання

- Переконайтеся, що у вашій системі встановлено Python.
- Завантажте або клонуйте репозиторію, а потім перейдіть до каталогу `py-mailer`.
- Встановіть необхідні залежності:
  ```shell
  pip install -r requirements.txt
  ```
- Підготуйте файл `templates.json` з шаблонами листів. Приклад налаштувань шаблонів знаходиться в файлі `templates.json.dist`
- Можна використовувати **змінні**, поставивши перед ними знак `$`.

  > Привіт $NAME, Ваш рахунок $price грн. за $months місяців

- Помістіть свої дані у `csv` файл
  *Перший рядок, тобто заголовки, повинен містити параметр "EMAIL" (в верхньому регістрі).*

  ![csv_image](./doc/csv_example.png)

  *Ви можете експортувати файл CSV з Microsoft Office Excel, Libre Office, Google Sheets, SQL Database або NoSQL Database*

- Створіть файл `config.json` та вкажіть налаштування для поштових акаунтів, з яких буде відбуватись відправка листів.

  Обов’язково введіть реальні значення. Наведені в файлі `config.json.dict` значення є лише прикладом.
- **Не вводьте оригінальний пароль електронної пошти.** 
  Створіть обліковий запис Gmail, потім увімкніть двоетапну перевірку, а потім налаштуйте [Пароль програми](https://support.google.com/accounts/answer/185833?hl=uk) для `py-mailer`. 

- Все налаштовано 👍. Можна запускати розсилку:
  ```shell
  python3 send.py --data-path=data.csv
  ```
  `data.csv` - шлях до файлу з даними

- По завершенню роботи програма видась результати з кількості відправлених листів.

## Потрібна допомога?

Будь ласка, повідомте про проблему або задайте своє запитання в розділі проблем репозиторію.
