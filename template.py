
import os
from typing import List
from email.mime.image import MIMEImage


class InvalidTemplateArgumentException(Exception):
    ...


class Template(object):
    __subject: str
    __txt_body: str = None
    __html_body: str = None
    __attachments: dict
    __attachments_list: dict = None
    __embed_images: dict
    __embed_images_list: List = []

    def __init__(self, config: dict):
        self.__init_properties(config)

    @property
    def subject(self):
        return self.__subject

    @property
    def txt_body(self):
        return self.__txt_body

    @property
    def html_body(self):
        return self.__html_body

    @property
    def attachments(self):
        return self.__attachments_list

    @property
    def embed_images(self):
        return self.__embed_images_list

    def __init_properties(self, config: dict):
        try:
            self.__subject = config.get('subject')
            assert self.__subject, 'Subject not defined'

            self.__txt_body = self.__get_content(config.get('txt_body'))
            self.__html_body = self.__get_content(config.get('html_body'))
            assert self.__txt_body or self.__html_body, 'Message body not defined'

            if not self.__html_body:
                self.__html_body = "<br />".join(self.__txt_body.split("\n"))

            self.__attachments = config.get('attachments', {})
            self.__embed_images = config.get('embed_images', {})

            self.__init_attachments()
            self.__init_embed_images()

        except AssertionError as e:
            raise InvalidTemplateArgumentException(str(e))

    def __get_content(self, txt):
        if not txt:
            return txt

        path = '{}/{}'.format(os.getcwd(), txt.rstrip('/'))

        if os.path.exists(path):
            fp = open(path, 'r')
            txt = fp.read()
            fp.close()

        return txt

    def __init_attachments(self) -> List:
        file_contents = []
        file_names = []

        for filename in self.__attachments:
            path = str(self.__attachments[filename])
            path = path.rstrip('/')
            fname = '{}/{}'.format(os.getcwd(), path)

            try:
                fp = open(fname, 'rb')

                file_names.append(filename)
                file_contents.append(fp.read())

                fp.close()
            except Exception:
                print(f'Attachment {fname} not found')

        if len(file_names):
            self.__attachments_list = {
                'names': file_names,
                'contents': file_contents
            }

    def __init_embed_images(self):
        self.__embed_images_list = []

        for key in self.__embed_images:
            path = str(self.__embed_images[key])
            path = path.rstrip('/')
            fname = '{}/{}'.format(os.getcwd(), path)

            if not fname.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif')):
                continue

            try:
                fp = open(fname, 'rb')
                msgImage = MIMEImage(fp.read())
                fp.close()

                msgImage.add_header('Content-ID', f'<{key}>')
                msgImage.add_header(
                    'Content-Disposition',
                    'inline; filename={}'.format(key)
                )
                self.__embed_images_list.append(msgImage)
            except Exception:
                print(f'File {fname} not found')
